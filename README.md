# AspDotnetMVCAssignmentOne

<strong>Description</strong>
<p>This assignment consist of a user registration form, login form and user detail page.</p>

<strong>Services Used</strong>
<ul>
    <li>Authentication Service (Provide basic authentication)</li>
    <li>Current User Service (Provide current authenticated user)</li>
    <li>User Service (Provide basic user related operations)</li>
    <li>File Upload Service (Helps to upload file in a server)</li>
    <li>Admin Action Service (Helps to perform admin specific actions)</li>
</ul>

<strong>Custom Action Filters</strong>
<ul>
    <li>Request Type Ajax Filter (Check whether the request is ajax request or not)</li>
    <li>User Login Filter (Check wheteher the user is already loged in or not)</li>
    <li>Authorize Filters (Check Whether the current user is admin or not)</li>
</ul>

<strong>Custom Helpers</strong>
<ul>
    <li>ImageUploader (Used To Upload Image)</li>
    <li>ImageRenderer (Used To Render Image)</li>
</ul>

<strong>Custom Attributes</strong>
<ul>
    <li>Check All Contacts (Check the validity of all user contacts)</li>
    <li>Unique Email (Check the email is already present or not)</li>
    <li>Valid Contact Type (Check the validity of contact types)</li>
    <li>Valid Image (Check the validity of image file provided by the user)</li>
</ul>

<strong>Data Models</strong>
<ul>
    <li>Contact</li>
    <li>Contact Type</li>
    <li>User</li>
    <li>Role</li>
</ul>

<strong>Controllers</strong>
<ul>
    <li>Account</li>
    <li>Check Email</li>
    <li>Contact Type</li>
    <li>Error</li>
    <li>Home</li>
    <li>User</li>
    <li>Admin</li>
</ul>

<strong>Other Information</strong>
<ul>
    <li>Form Authentication is used</li>
    <li>Code First Approach is used</li>
    <li>Entity Framework is used</li>
    <li>Crypto is used for encryption of user passowrd</li>
    <li>Unity.MVC4 is used for dependency injection</li>
    <li>Jquery Validation is used for client side validations</li>
    <li>All views are responsive</li>
    <li>There is a admin user</li>
    <li>Admin will able to view all user details</li>
    <li>Admin can delete a specific user</li>
    <li>Admin can edit basic information of an existing user</li>
</ul>