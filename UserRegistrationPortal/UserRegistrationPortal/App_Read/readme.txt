﻿>> Naming conventions <<
C# Methods, Interface &  Classes : Pascal Case
C# Properties : Pascal Case
C# Local Variables : Camel Case
Javascript Methods & Variables : Camel Case
HTML attributes like (id & name) : Camel Case
CSS Class name : Kebab Case

>> Design Pattern <<
DI For Services And Controllers

>> Target Framework <<
.NET Framework 4.5.2