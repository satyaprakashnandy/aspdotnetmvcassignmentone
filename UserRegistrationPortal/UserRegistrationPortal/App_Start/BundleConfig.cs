﻿using System.Web.Optimization;

namespace UserRegistrationPortal.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // + Style Bundles +

            StyleBundle RegisterStyleBundle = new StyleBundle("~/Bundles/RegisterStyleBundle");
            RegisterStyleBundle.Include("~/Content/Registration/AccountRegister.css");
            bundles.Add(RegisterStyleBundle);

            StyleBundle HomeStyleBundle = new StyleBundle("~/Bundles/HomeStyleBundle");
            HomeStyleBundle.Include("~/Content/Home/HomeIndex.css");
            bundles.Add(HomeStyleBundle);

            StyleBundle LoginStyleBundle = new StyleBundle("~/Bundles/LoginStyleBundle");
            LoginStyleBundle.Include("~/Content/Login/AccountLogin.css");
            bundles.Add(LoginStyleBundle);

            StyleBundle UserStyleBundle = new StyleBundle("~/Bundles/UserStyleBundle");
            UserStyleBundle.Include("~/Content/User/UserIndex.css", "~/Content/User/UpdateModelPopUp.css", "~/Content/PopUp.css");
            bundles.Add(UserStyleBundle);

            StyleBundle LayoutStyleBundle = new StyleBundle("~/Bundles/LayoutStyleBundle");
            LayoutStyleBundle.Include("~/Content/Site.css");
            bundles.Add(LayoutStyleBundle);

            StyleBundle ErrorStyleBundle = new StyleBundle("~/Bundles/ErrorStyleBundle");
            ErrorStyleBundle.Include("~/Content/Error/Error.css");
            bundles.Add(ErrorStyleBundle);

            
            StyleBundle AdminStyleBundle = new StyleBundle("~/Bundle/AdminStyleBundle");
            AdminStyleBundle.Include("~/Content/AllRecords/AccountAllRecords.css", "~/Content/AllRecords/ViewModelPopUp.css" , "~/Content/AllRecords/EditModelPopUp.css" , "~/Content/PopUp.css");
            bundles.Add(AdminStyleBundle);

            // + Script Bundles +

            ScriptBundle RegisterScriptBundle = new ScriptBundle("~/Bundles/RegisterScriptBundle");
            RegisterScriptBundle.Include("~/scripts/jquery-3.3.1.min.js",
                "~/scripts/jquery.validate.min.js",
                "~/scripts/CustomHelperMethods/CustomHelperMethods.js",
                "~/scripts/RegistrationScripts/Validation.js",
                "~/scripts/RegistrationScripts/CustomValidation.js",
                "~/scripts/RegistrationScripts/EventRegister.js",
                "~/scripts/RegistrationScripts/AddElement.js",
                "~/scripts/AjaxCalls/UserRegistrationPortalAjaxCall.js",
                "~/scripts/RegistrationScripts/Main.js",
                "~/scripts/additional-methods.min.js");
            bundles.Add(RegisterScriptBundle);

            ScriptBundle UserModelPopUpBundle = new ScriptBundle("~/Bundles/UserModelPopUp");
            UserModelPopUpBundle.Include("~/scripts/jquery-3.3.1.min.js", 
                "~/scripts/jquery.validate.min.js", 
                "~/scripts/RegistrationScripts/Validation.js", 
                "~/scripts/UserControllerScripts/CustomValidation.js",
                "~/scripts/AjaxCalls/UserRegistrationPortalAjaxCall.js",
                "~/scripts/UserControllerScripts/FormAction.js",
                "~/scripts/UserControllerScripts/ModelAction.js",
                "~/scripts/UserControllerScripts/EventRegister.js",
                "~/scripts/UserControllerScripts/Main.js");
            bundles.Add(UserModelPopUpBundle);

            ScriptBundle AllRecordsBundle = new ScriptBundle("~/Bundles/AllRecords");
            AllRecordsBundle.Include("~/scripts/jquery-3.3.1.min.js",
                "~/scripts/jquery.validate.min.js",
                "~/scripts/RegistrationScripts/Validation.js",
                "~/scripts/CustomHelperMethods/CustomHelperMethods.js",
                "~/scripts/AllRecords/CustomValidation.js",
                "~/scripts/AjaxCalls/UserRegistrationPortalAjaxCall.js",
                "~/scripts/RegistrationScripts/Validation.js",
                "~/scripts/AllRecords/Actions.js", 
                "~/scripts/AllRecords/EventRegister.js", 
                "~/scripts/AllRecords/Main.js");
            bundles.Add(AllRecordsBundle);

            BundleTable.EnableOptimizations = true;
        }
    }
}