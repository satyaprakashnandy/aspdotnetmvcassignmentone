﻿using System.Linq;
using System.Web.Mvc;
using UserRegistrationPortal.Filters;
using UserRegistrationPortal.Services;
using UserRegistrationPortal.Models;
using UserRegistrationPortal.Dal;

namespace UserRegistrationPortal.Controllers
{
    public class AdminController : Controller
    {
        private IUserService userService;
        private ICurrentUserService currentUserService;
        private IAdminActionsService adminActionService;
        private UserRegistrationPortalContext context;
        public AdminController(IUserService userService,ICurrentUserService currentUserService,IAdminActionsService adminActionService,UserRegistrationPortalContext context)
        {
            this.userService = userService;
            this.currentUserService = currentUserService;
            this.adminActionService = adminActionService;
            this.context = context;
        }

        /// <summary>
        /// Returns All User Record (Only For Admin Roles)
        /// </summary>
        [HttpGet]
        [Authorize]
        [AuthorizeFilter(role:"admin")]
        public ActionResult AllRecords()
        {
            ViewBag.AllUsers = adminActionService.GetAllRecords(currentUserService.GetCurrentUser(Request).RoleId);
            return View();
        }

        /// <summary>
        /// Get Full Detail of a user with an appropriate id (Only For Admin Roles)
        /// </summary>
        [HttpGet]
        [RequestTypeAjaxFilter]
        [Authorize]
        [AuthorizeFilter(role: "admin")]
        public JsonResult GetUser(int id)
        {
            var result = userService.GetUserDetails(context.User.Find(id));
            return Json(adminActionService.JsonCreater(result), "application/json", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update any user detail with an appropriate id (Only For Admin Roles)
        /// </summary>
        [HttpPut]
        [RequestTypeAjaxFilter]
        [Authorize]
        [AuthorizeFilter(role: "admin")]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateUser(int id,UserUpdateViewModels userDetails)
        {
            if (ModelState.IsValid)
            {
                var user = context.User.Include("UserRole").Where(u => u.Id == id).FirstOrDefault();
                return Json(userService.UpdateUserDetails(userDetails, user), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Delete any user detail with an appropriate id (Only For Admin Roles)
        /// </summary>
        [HttpDelete]
        [RequestTypeAjaxFilter]
        [Authorize]
        [AuthorizeFilter(role: "admin")]
        public JsonResult DeleteUser(int id)
        {
            return Json(userService.DeleteUser(id));
        }
    }
}