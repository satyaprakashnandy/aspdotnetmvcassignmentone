﻿using System.Web.Mvc;
using UserRegistrationPortal.Services;
using UserRegistrationPortal.Filters;
using UserRegistrationPortal.Models;

namespace UserRegistrationPortal.Controllers
{
    public class UserController : Controller
    {
        private ICurrentUserService currentUser;
        private IUserService userService;
        public UserController(ICurrentUserService currentUser, IUserService userService)
        {
            this.currentUser = currentUser;
            this.userService = userService;
        }
        /// <summary>
        ///     Get user details of an authenticated user.
        /// </summary>
        /// <returns>User detail view for an authenticated user</returns>
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            return View(userService.GetUserDetails(currentUser.GetCurrentUser(Request)));
        }
        /// <summary>
        ///     Update authenticated user information.
        /// </summary>
        /// <param name="userDetails">Accept UserUpdateViewModels</param>
        /// <returns>True or False</returns>
        [HttpPut]
        [RequestTypeAjaxFilter]
        [Authorize]
        [ValidateAntiForgeryToken]
        public JsonResult Update(UserUpdateViewModels userDetails)
        {
            if (ModelState.IsValid)
            {
                var user = currentUser.GetCurrentUser(Request);
                return Json(userService.UpdateUserDetails(userDetails, user), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}