﻿using System.Web.Mvc;
using System.Web.Routing;
using UserRegistrationPortal.Services;
using UserRegistrationPortal.Dal;

namespace UserRegistrationPortal.Filters
{
    public class AuthorizeFilter : ActionFilterAttribute
    {
        public string Role { get; set; }
        private ICurrentUserService currentUserService = new CurrentUserServiceImplementation(new UserRegistrationPortalContext());

        public AuthorizeFilter()
        {

        }
        public AuthorizeFilter(string role)
        {
            this.Role = role;
        }
        /// <summary>
        /// Check whether the request is from admin or not
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (var context = new UserRegistrationPortalContext())
            {
                if (!(context.Role.Find(currentUserService.GetCurrentUser(filterContext.HttpContext.Request).RoleId).RoleType == Role))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Index", controller = "User" }));
                }
            }
        }
    }
}