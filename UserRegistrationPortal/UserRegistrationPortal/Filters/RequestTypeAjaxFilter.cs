﻿using System.Web;
using System.Web.Mvc;

namespace UserRegistrationPortal.Filters
{
    public class RequestTypeAjaxFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Check whether the request is ajax request or not.
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                throw new HttpException(404, "Page Not Found");
            }
        }
    }
}