﻿using System.Collections.Generic;
using System.Linq;
using UserRegistrationPortal.Dal;
using UserRegistrationPortal.Models;

namespace UserRegistrationPortal.Services
{
    public class AdminActionsServiceImplementation : IAdminActionsService
    {
        private IUserService userService;
        private UserRegistrationPortalContext context;
        public AdminActionsServiceImplementation(IUserService userService,UserRegistrationPortalContext context)
        {
            this.userService = userService;
            this.context = context;
        }
        public List<UserViewModels> GetAllRecords(int id)
        {
            List<UserViewModels> allUsersRecords = new List<UserViewModels>();
            var result = context.User.Where(u => u.RoleId != id);
            foreach (var user in result)
            {
                allUsersRecords.Add(userService.GetUserDetails(user));
            }
            return allUsersRecords;
        }

        public Dictionary<string, object> JsonCreater(UserViewModels user)
        {
            Dictionary<string, object> userDetails = new Dictionary<string, object>();
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("FirstName", user.FirstName);
            details.Add("LastName", user.LastName);
            details.Add("Email", user.Email);
            details.Add("Image", user.Image);
            details.Add("Address", user.Address);
            userDetails.Add("Details", details);
            Dictionary<string, List<string>> contacts = new Dictionary<string, List<string>>();
            foreach (var contact in user.Contact)
            {
                List<string> contactWithType = new List<string>();
                contactWithType.Add(contact.ContactType.ContactTypeText);
                contactWithType.Add(contact.ContactNumber);
                contacts.Add(contact.Id.ToString(), contactWithType);
            }
            userDetails.Add("Contacts", contacts);
            return userDetails;
        }
    }
}