﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserRegistrationPortal.Models;

namespace UserRegistrationPortal.Services
{
    public interface IAdminActionsService
    {
        /// <summary>
        /// Get All user records except admin records.
        /// </summary>
        /// <returns>List of all user records as a user view model</returns>
        List<UserViewModels> GetAllRecords(int id);
        /// <summary>
        /// Json Creater Responsible To Create Appropriate JSON Format For User Details.
        /// </summary>
        /// <returns>Dictionary Of Dictionary</returns>
        Dictionary<string, object> JsonCreater(UserViewModels user);
    }
}
