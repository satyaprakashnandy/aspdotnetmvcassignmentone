﻿using UserRegistrationPortal.Models;

namespace UserRegistrationPortal.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Add User Details (For Registration)
        /// </summary>
        bool AddUserDetails(UserRegisterViewModels userRegistrationDetails);

        /// <summary>
        /// Get User Details
        /// </summary>
        UserViewModels GetUserDetails(User user);

        /// <summary>
        /// Update User Information
        /// </summary>
        /// <returns>True or False</returns>
        bool UpdateUserDetails(UserUpdateViewModels userUpdateDetails, User currentUser);

        /// <summary>
        /// Delete A Specific User
        /// </summary>
        /// <returns>True/False</returns>
        bool DeleteUser(int id);
    }
}
