﻿'use strict';

// >> Ajax call templates

var userRegistrationPortalAjaxCall = {

    // >> [ Method : GET ]

    ajaxGetCall: function (url) {
        return $.ajax({
            url: url,
            method: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json"
        })
    },

    // >> [ Method : POST ]

    ajaxPostCall: function (url, data) {
        return $.ajax({
            url: url,
            method: "POST",
            contentType: "application/json;charset=utf-8",
            data: data
        })
    },

    // >> [ Method : PUT ]

    ajaxUpdateCall: function (url, data) {
        return $.ajax({
            url: url,
            method: "PUT",
            contentType:'application/x-www-form-urlencoded; charset=utf-8',
            data: data
        })
    },

    // >> [ Method : DELETE ]

    ajaxDeleteCall: function (url) {
        return $.ajax({
            url: url,
            method: "DELETE",
            contentType: "application/json;charset=utf-8",
            dataType: "json"
        })
    }
}