﻿var actions = {

    // >> Responsible to get user details.

    getUserDetails : function(id)
    {
        // >> [ Method : GET, Controller : Admin, Action : GetUser ]

        userRegistrationPortalAjaxCall.ajaxGetCall("/Admin/GetUser/" + id).done(function (data) {

            $("#userImageView").attr("src", data["Details"]["Image"].replace("~/", "/"));

            $("#firstNameView").html(data["Details"]["FirstName"]);

            $("#lastNameView").html(data["Details"]["LastName"]);

            $("#emailView").html(data["Details"]["Email"]);

            $("#addressView").html(data["Details"]["Address"]);

            // >> Make Contact List Empty

            $("#contactList").html("");

            $.each(Object.keys(data["Contacts"]), function (key, value) {
                $("#contactList").append("<li><small>[" + data["Contacts"][value][0] + "]</small> " + data["Contacts"][value][1] + "</li>");
            })

            $(".model-view").fadeIn(500);

        }).fail(function () {
            
            alert("Sorry Something Went Wrong ...");

        });
    },

    // >> Responsible to update user details.

    updateUser : function(id)
    {
        actions.showStatus("Processing Your Request ...");

        let formData = { "__RequestVerificationToken": $('input[name="__RequestVerificationToken"]').val(), "FirstName": $("#firstName").val(), "LastName": $("#lastName").val(), "Address": $("#address").val() }

        // >> [ Method : PUT, Controller : Admin, Action : UpdateUser ]

        userRegistrationPortalAjaxCall.ajaxUpdateCall("/Admin/UpdateUser/" + id, formData).done(function (data) {

            if (data)
            {
                actions.showStatus("Data Updated Successfully");

                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
            else
            {
                actions.showStatus("Sorry Something Went Wrong !");
            }

        }).fail(function () {
            actions.showStatus("Sorry Something Went Wrong !");
        });
    },

    // >> Responsible to delete a specific user

    deleteUser : function(id)
    {
        // >> [ Method : DELETE, Controller : Admin, Action : DeleteUser ]

        userRegistrationPortalAjaxCall.ajaxDeleteCall("/Admin/DeleteUser/" + id).done(function (data) {

            alert("Data Deleted Successfully !");

            location.reload();
      
        }).fail(function () {
            alert("Sorry Something Went Wrong...");
        });
    },

    // >> Get Update Details of a specific user.

    getUpdateUserDetails : function(id)
    {
        // >> [ Method : GET, Controller : Admin, Action : GetUser ]

        userRegistrationPortalAjaxCall.ajaxGetCall("/Admin/GetUser/" + id).done(function (data) {
            $("#firstName").val(data["Details"]["FirstName"]);
            $("#lastName").val(data["Details"]["LastName"]);
            $("#address").val(data["Details"]["Address"]);
            $("#updateButton").attr("data-id", id);
            $("#userUpdateForm").attr("data-id", id);
            $(".model-edit").fadeIn(500);
        }).fail(function () {

            alert("Sorry Something Went Wrong ...");

        });
    },

    // >> Show appropriate message

    showStatus: function (message) {

        $("#updateStatus").html(message);

    },
}