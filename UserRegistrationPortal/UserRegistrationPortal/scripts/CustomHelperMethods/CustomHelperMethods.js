﻿'use strict';

// >> Add extension method to string in oder to format a specific string

String.prototype.format = function () {
    let stringValue = this;
    for(let index in arguments)
    {
        stringValue = stringValue.replace("{" + index + "}", arguments[index]);
    }
    return stringValue;
}