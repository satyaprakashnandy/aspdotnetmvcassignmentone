﻿'use strict';
var customValidation = {

    // >> Add Custom Validation

    addCustomValidation: function () {
        $("#userUpdateForm").validate({

            rules: validation.rules,
            messages: validation.messages,
            submitHandler: function (form) {
                formAction.updateUser();
            }
        });
    },

    // >> Add Add validation methods

    addCustomValidationMethods: function () {
        $.validator.addMethod("allLetters", function (value) {
            return validation.allLetters(value);
        });
        $.validator.addMethod("lengthWithoutSpace", function (value) {
            return validation.underflow(value);
        });
    }
}