﻿'use strict';

// >> Form action responsible for update user and show appropriate messages.

var formAction = {

    // >> Responsible for update user information.

    updateUser : function()
    {
        // >> Show message that user request is in process

        formAction.showStatus("Processing Your Request ...");

        // >> User information for update.

        let formData = { "__RequestVerificationToken": $('input[name="__RequestVerificationToken"]').val(), "FirstName": $("#firstName").val(), "LastName": $("#lastName").val(), "Address": $("#address").val() }

        // >> [ Method : PUT, Controller : User, Action : Update ]

        userRegistrationPortalAjaxCall.ajaxUpdateCall("/User/Update/", formData).done(function (data) {

            if (data)
            {
                formAction.showStatus("Data updated successfully !");

                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
            else
            {
                formAction.showStatus("Sorry Something Went Wrong !");
            }
            

        }).fail(function () {

            formAction.showStatus("Sorry Something Went Wrong !");

        });
    },

    // >> Responsible to show user friendly status messages.

    showStatus: function (message) {

        $("#updateStatus").html(message);

    }
}