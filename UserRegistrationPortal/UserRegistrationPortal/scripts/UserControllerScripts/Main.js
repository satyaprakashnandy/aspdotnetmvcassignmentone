﻿'use strict';

$(document).ready(function () {

    eventRegister.registerEvents();

    customValidation.addCustomValidation();

    customValidation.addCustomValidationMethods();

});