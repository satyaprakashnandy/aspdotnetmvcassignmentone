﻿'use strict';

var modelAction = {

    // >> Open Model Pop Up

    open : function()
    {
        $("#updateModel").fadeIn(500);
    },

    // >> Close Model Pop Up

    close : function()
    {
        $("#updateModel").fadeOut(500);
    }
}